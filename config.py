from configparser import ConfigParser
from glob import glob
from os.path import abspath
from os.path import dirname

basedir = abspath(dirname(__file__))

config = ConfigParser()
config_files = sorted(glob(f'{basedir}/config/*.conf'))

for config_file in config_files:
    config.read(config_file)

config_signer = config['signer']
SIGNER_TITLE = config_signer['title']
SIGNER_NAME = config_signer['name']
SIGNER_FINGERPRINT = config_signer['fingerprint']

config_smtp = config['smtp']
SMTP_SENDER = config_smtp['sender']
SMTP_USERNAME = config_smtp['username']
SMTP_HOST = config_smtp['host']
SMTP_PORT = config_smtp.getint('port')
